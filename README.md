# Spring Boot Rest Scaffold

<Intro>

- Frameworks:
	- Spring
	- SpringBoot
	- Hibernate
	- JUnit

- API:
    - GET /api/hello/ - test API
	- GET /api/model/{id} - get Model
	- POST /api/model/ - save Model
	- GET /api/model/ - list Models
	- DELETE /api/model/{id} - delete Model

## Getting Started

### Prerequisites

- Git

### Installing

Clone the repository into your machine via command line or directly in IDE.

## Running the application

```
./gradlew bootRun
```


## Testing if it works

Check endpoint:

```
localhost:8080/api/hello
```

or, for example, to list Model:


```
localhost:8080/api/model/
```

## Running the tests

Tests were made in the Controller layer.

```
./gradlew test
```

## Deployment

With SpringBoot, it's not necessary to generate a jar/war and deploy on a webserver. However, it can be done after build.

## Built With

* [Gradle](https://gradle.org/)

## Authors

* **Bruno Liberal** - [GitHub](https://github.com/brunoliberal) [BitBucket](https://bitbucket.org/brunoliberal)

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Acknowledgments and Project Decisions

* Configuration files can be a little messy to handle if you have so many that you cannot find what you want. I prefer annotations.
* SpringBoot have a amazing capability of doing easy things fast ans easy.
* Unit tests in the repository layer are not very useful when using spring CrudRepository interface.

