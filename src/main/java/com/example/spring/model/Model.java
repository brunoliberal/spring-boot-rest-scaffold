package com.example.spring.model;

import javax.persistence.*;

@Entity
@Table(name="model_table")
public class Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String property;

    public Model() {}

    public Model(Long id, String prop) {
        this.id = id;
        this.property = prop;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
