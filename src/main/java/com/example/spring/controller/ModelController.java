package com.example.spring.controller;

import com.example.spring.model.Model;
import com.example.spring.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/")
public class ModelController {

    @Autowired
    private ModelRepository repo;

    @GetMapping(path = "hello")
    public String helloWorld(){
        return "Yep, It's working.";
    }

    @GetMapping("model/{id}")
    public ResponseEntity<Model> get(@PathVariable String id) {
        return new ResponseEntity(repo.findById(Long.valueOf(id)), HttpStatus.OK);
    }

    @PostMapping("model/")
    public ResponseEntity<Model> insert(@RequestBody Model model) {
        return new ResponseEntity<>(repo.save(model), HttpStatus.OK);
    }

    @GetMapping("model/")
    public ResponseEntity<Iterable<Model>> list() {
        return new ResponseEntity<>(repo.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("model/{id}")
    public ResponseEntity<Model> delete(@PathVariable String id) {
        repo.deleteById(Long.valueOf(id));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
