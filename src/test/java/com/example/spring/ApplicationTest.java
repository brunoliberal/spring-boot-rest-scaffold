package com.example.spring;

import com.example.spring.model.Model;
import com.example.spring.repository.ModelRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

    @Autowired
    private TestRestTemplate restTemp;

    @Autowired
    private ModelRepository modelRepo;

    @Test
    public void testList() {
        ResponseEntity<ArrayList> resp = restTemp.getForEntity(URI.create("/api/model/"), ArrayList.class);
        ArrayList<Model> list = resp.getBody();
        assertThat("List correct number of entities", list, hasSize(5));
    }

    @Test
    public void testGet() {
        ResponseEntity<Model> resp = restTemp.getForEntity(URI.create("/api/model/1"), Model.class);
        Model model = resp.getBody();
        assertThat("Get correct entity have right id", model.getId(), equalTo(1L));
        assertThat("Get correct entity have right property", model.getProperty(), equalTo("model1"));
    }

    @Test
    public void testInsert() {
        Model model = new Model(6L, "model6");
        restTemp.put(URI.create("/api/model/"), model);
        assertThat("Model 6 is created", modelRepo.findById(6L), notNullValue());
    }

    @Test
    public void testDelete() {
        restTemp.delete(URI.create("/api/model/1"));
        assertThat("Model 1 is deleted", modelRepo.findById(1L), equalTo(Optional.empty()));
    }
}
